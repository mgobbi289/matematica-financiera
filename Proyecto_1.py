import random
from collections import defaultdict
from math import sqrt

# Matematica Financiera: Proyecto 1
# Garay, Lucas
# Gobbi, Matias

# METODO A

# Generacion de Bernoulli
# Tirada de Moneda
# Probabilidad de Cara = p
def bernoulli(p):
	u = random.random()
	if u < p:
		return 1
	else:
		return 0

# Simula una Trayectoria
# p: probabilidad riesgo neutral
def simular_trayectoria(S0, p, u, d, n):
	minimo = S0
	for _ in range(n):
		moneda = bernoulli(p)
		if moneda == 1:
			S0 = S0 * u # Cara
		else:
			S0 = S0 * d # Cruz
		minimo = min(minimo, S0)
	payoff = S0 - minimo
	return payoff

# METODO A: Montecarlo con N simulaciones

# Estima V0 con Montecarlo
# S0: precio subyacente
# u: up
# d: down
# i: tasa efectiva por período
# n: cantidad de pasos
# N: cantidad de simulaciones
def metodo_A_simulacion(S0, u, d, i, n, N):
	# Probabilidad Riesgo Neutral
	p = (1+i-d)/(u-d)
	# Montecarlo
	esperanza = 0
	for _ in range(N):
		payoff = simular_trayectoria(S0, p, u, d, n)
		esperanza += payoff
	esperanza = esperanza / N
	# Calcular Prima
	descuento = (1+i)**n
	V0 = esperanza / descuento
	print("Metodo A con N = " + str(N))
	print("Precio de Prima: " + str(V0))

# METODO A: Montecarlo con Desviación < delta

# Estima V0 con Montecarlo
# Corre simulaciones hasta que la desviacion
# muestral del estimador sea menor a delta
# delta: cota para la desviación muestral del estimador
def metodo_A_desviacion(S0, u, d, i, n, delta):
	# Probabilidad Riesgo Neutral
	p = (1+i-d)/(u-d)
	# Montecarlo
	media = simular_trayectoria(S0, p, u, d, n)
	Scuad = 0
	N = 1
	while N <= 100 or sqrt(Scuad/N) > delta:
	    N += 1
	    payoff = simular_trayectoria(S0, p, u, d, n)
	    media_previa = media
	    media = media + (payoff - media) / N
	    Scuad = Scuad * (1-1/(N-1)) + N * (media-media_previa)**2
	# Calcular Prima
	descuento = (1+i)**n
	V0 = media / descuento
	print("Metodo A con delta = " + str(delta))
	print("Cantidad Simulaciones: " + str(N))
	print("Precio de Prima: " + str(V0))

# METODO B

# Calcula nueva lista de minimos
# hijos: lista valores subyacente
# minimos: lista de diccionarios
# k: valor minimo en trayectoria
# v: cantidad de apariciones
def calcular_minimos(hijos, minimos):
	n = len(hijos)
	# Primer Hijo
	nuevos_minimos = [minimos[0]]
	for i in range(1, n-1):
		Si = hijos[i]
		aux = defaultdict(float)
		# Comparacion de Minimos
		for k, v in minimos[i-1].items():
			# Padre Superior
			aux[min(k, Si)] += v
		for k, v in minimos[i].items():
			# Padre Inferior
			aux[min(k, Si)] += v
		nuevos_minimos.append(aux)
	# Ultimo Hijo
	aux = defaultdict(float)
	aux[hijos[n-1]] = 1
	nuevos_minimos.append(aux)
	return nuevos_minimos

# Calcula la esperanza del payoff
# p: probabilidad riesgo neutral
def calcular_esperanza(hijos, minimos, p):
	n = len(hijos)
	esperanza = 0
	q = 1 - p
	for i in range(n):
		Si = hijos[i]
		dict_min = minimos[i]
		for k, v in dict_min.items():
			prob = (p**(n-1-i)) * (q**i) # Probabilidad Trayectoria
			esperanza += (Si-k) * v * prob
	return esperanza

# Calcula V0 usando lista de minimos
# S0: precio subyacente
# u: up
# i: tasa efectiva por periodo
# n: cantidad pasos
def metodo_B(S0, u, i, n):
	d = 1/u
	# Lista de Padres
	padres = [S0]
	aux = defaultdict(float)
	aux[S0] = 1
	# Lista de Minimos
	minimos = [aux]
	for _ in range(n):
		hijos = []
		# Primer Hijo
		hijos.append(padres[0]*u)
		for padre in padres:
			Sd = padre*d
			hijos.append(Sd)
		# Calculo nueva lista de minimos
		minimos = calcular_minimos(hijos, minimos)
		# Avanzo un paso
		padres = hijos
	# Calcular Prima
	p = ((1+i)-d)/(u-d)
	esperanza = calcular_esperanza(hijos, minimos, p)
	descuento = (1+i)**n
	V0 = esperanza / descuento
	print("Metodo B ")
	print("Precio de Prima: " + str(V0))