from scipy.stats import norm
from math import sqrt, exp

# Matematica Financiera: Proyecto 2
# Garay, Lucas
# Gobbi, Matias

# Opciones Bermudas

# Simula valores del activo en períodos
# T/3, 2T/3, T
# m: es la media del movimiento
# s: es la desviacion del movimiento
def simular_activo(S0, T, r, sigma):
	tendencia = r - (sigma**2)/2
	t = T / 3
	m = tendencia * t
	s = sigma * sqrt(t)
	# Calcula S1
	w = norm.rvs(loc=m, scale=s)
	S1 = S0 * exp(w)
	# Calcula S2
	w = norm.rvs(loc=m, scale=s)
	S2 = S1 * exp(w)
	# Calcula S3
	w = norm.rvs(loc=m, scale=s)
	S3 = S2 * exp(w)
	return (S1, S2, S3)

# Simula n trayectorias del activo
def simular_trayectorias(S0, T, r, sigma, n):
	trayectorias = []
	for i in range(n):
		simulacion = simular_activo(S0, T, r, sigma)
		trayectorias.append(simulacion)
	return trayectorias

# Calcula payoff de ejercer en período 3
# Devuelve una lista con los payoffs de cada simulacion
def ejercer_en_3(trayectorias, K):
	valores = []
	for precios in trayectorias:
		payoff = max(K - precios[2], 0)
		valores.append(payoff)
	return valores

# Valores de ejercer/esperar en período 2 (dada una cota)
# Devuelve una lista con los valores de la opcion para cada simulacion
def ejercer_en_2(trayectorias, K, r, T, cota, esperar):
	valores = []
	n = len(trayectorias)
	for i in range(n):
		# Tomo S2
		precio = trayectorias[i][1]
		if precio <= cota and precio < K:
			# Ejercicio
			payoff = K - precio
		else:
			# Esperar / Descuento
			payoff = esperar[i] / exp(r * T / 3)
		valores.append(payoff)
	return valores

# Valores de ejercer/esperar en período 1 (dada una cota)
# Devuelve una lista con los valores de la opcion para cada simulacion
def ejercer_en_1(trayectorias, K, r, T, cota, esperar):
	valores = []
	n = len(trayectorias)
	for i in range(n):
		# Tomo S1
		precio = trayectorias[i][0]
		if precio <= cota and precio < K:
			# Ejercicio
			payoff = K - precio
		else:
			# Esperar / Descuento
			payoff = esperar[i] / exp(r * T / 3)
		valores.append(payoff)
	return valores

# Algoritmo de Valoración de Opciones Bermudas
# S0: precio inicial del activo
# T: tiempo de madurez(medido en años)
# r: tasa continua anual
# sigma: volatilidad del activo
# K: prima de la opcion
# n: cantidad de simulaciones para determinar los cortes
# N: cantidad de simulaciones para determinar la prima
def valorar_bermudas(S0, T, r, sigma, K, n, N):
	trayectorias = simular_trayectorias(S0, T, r, sigma, n)

	# Calcula el payoff de ejercer al final
	valores_en_3 = ejercer_en_3(trayectorias, K)
	cota_en_3 = K
	print("Corte en Período 3: ")
	print(cota_en_3)

	# Calcula la cota S2*
	cota_en_2 = 0
	valores_en_2 = ejercer_en_2(trayectorias, K, r, T, cota_en_2, valores_en_3)
	maximo_V = sum(valores_en_2) / len(valores_en_2)
	# Reviso todos los candidatos
	for precios in trayectorias:
		cota = precios[1]
		valores = ejercer_en_2(trayectorias, K, r, T, cota, valores_en_3)
		promedio = sum(valores) / len(valores)
		if promedio > maximo_V:
			# Nuevo Maximo
			maximo_V = promedio
			valores_en_2 = valores
			cota_en_2 = cota
	print("Corte en Período 2: ")
	print(cota_en_2)

	# Calcula la cota S1*
	cota_en_1 = 0
	valores_en_1 = ejercer_en_1(trayectorias, K, r, T, cota_en_1, valores_en_2)
	maximo_V = sum(valores_en_1) / len(valores_en_1)
	# Reviso todos los candidatos
	for precios in trayectorias:
		cota = precios[0]
		valores = ejercer_en_1(trayectorias, K, r, T, cota, valores_en_2)
		promedio = sum(valores) / len(valores)
		if promedio > maximo_V:
			# Nuevo Maximo
			maximo_V = promedio
			valores_en_1 = valores
			cota_en_1 = cota
	print("Corte en Período 1: ")
	print(cota_en_1)

	# Ya fije los valores de Si*
	# Ahora vuelvo a simular trayectorias del activo
	trayectorias = simular_trayectorias(S0, T, r, sigma, N)
	# Calculo los precios de la opcion entre períodos
	valores_en_3 = ejercer_en_3(trayectorias, K)
	precio_en_3 = sum (valores_en_3) / len(valores_en_3)
	valores_en_2 = ejercer_en_2(trayectorias, K, r, T, cota_en_2, valores_en_3)
	precio_en_2 = sum(valores_en_2) / len(valores_en_2)
	valores_en_1 = ejercer_en_1(trayectorias, K, r, T, cota_en_1, valores_en_2)
	precio_en_1 = sum(valores_en_1) / len(valores_en_1)

	# Finalmente, calculo V0
	descuento_en_1 = precio_en_1 / exp(r * T / 3)
	V0 = max(K - S0, descuento_en_1)
	print("Prima Calculada: ")
	print(V0)

# Ejemplo del Informe
S0 = 36
K = 35
T = 1
r = 0.06
sigma = 0.2
n = 100
N = 2000
valorar_bermudas(S0, T, r, sigma, K, n, N)